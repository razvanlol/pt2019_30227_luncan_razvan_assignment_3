# Restaurant Simulator
## In Order to run this project you will need to install MySQLWorkbench

### Actions performed:

#### On Clients:
* View All
* Insert
* Delete
* Update

#### On Products:
* View All
* Insert
* Delete
* Update

#### On Orders:
* Insert
* Process Order
