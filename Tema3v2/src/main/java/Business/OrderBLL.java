package Business;


import Business.Validators.OrderDateValidation;
import DAO.OrderDAO;
import Model.Order;

public class OrderBLL {

    public int insertOrder(Order order) {
        new OrderDateValidation().validate(order);
        return OrderDAO.insert(order);
    }
}
