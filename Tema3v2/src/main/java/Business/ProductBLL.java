package Business;

import Business.Validators.ProductValidation;
import DAO.ProductDAO;
import Model.Product;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class ProductBLL {


    public Product findProductById(int id) {
        Product st = ProductDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    public void updateProduct(int id,String name,int price,int quantity){
        ProductDAO.updateById(id,name,price,quantity);
    }

    public int insertProduct(Product product) {
        new ProductValidation().validate(product);
        return ProductDAO.insert(product);
    }

    public ArrayList<Product> viewAll(){
        ArrayList<Product> products=ProductDAO.viewAll();
      
        return products;
    }

    public void deleteProduct(int id){
        ProductDAO.deleteById(id);
    }

}
