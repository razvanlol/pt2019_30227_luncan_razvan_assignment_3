package Business.Validators;

import Business.Validators.Validator;
import Model.Order;

public class OrderDateValidation implements Validator<Order> {

    private static final int MIN_DAY=1;
    private static final int MAX_DAY=31;

    private static final int MIN_MONTH=1;
    private static final int MAX_MONTH=12;

    private static final int MIN_YEAR=1970;
    private static final int MAX_YEAR=2019;

    public void validate(Order t){

        if(t.getDay()<MIN_DAY || t.getDay()>MAX_DAY){
            throw new IllegalArgumentException("The day is not a valid one.");
        }

        if(t.getMonth()<MIN_MONTH || t.getMonth()>MAX_MONTH){
            throw new IllegalArgumentException("The month is not a valid one.");
        }

        if(t.getYear()<MIN_YEAR || t.getMonth()>MAX_YEAR){
            throw new IllegalArgumentException("The year is not a valid one.");
        }

    }


}
