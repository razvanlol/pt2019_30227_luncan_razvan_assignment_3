package Business.Validators;

import Model.Product;

public class ProductValidation implements Validator<Product> {

    @Override
    public void validate(Product product) {
        if(product.getPrice()<=0 || product.getQuantity()<=0){
            throw new IllegalArgumentException("The Product you have entered is not a valid one.");
        }
    }
}
