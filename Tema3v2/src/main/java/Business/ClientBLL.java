package Business;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import Business.Validators.ClientAgeValidation;
import Business.Validators.EmailValidator;
import Business.Validators.Validator;
import DAO.ClientDAO;
import Model.Client;

public class ClientBLL {

    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        validators.add(new ClientAgeValidation());
    }

    public Client findClientById(int id) {
        Client st = ClientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return st;
    }

    public void updateClient(int id,String name,String address,String email,int age){
        boolean reusit=ClientDAO.updateById(id,name,address,email,age);
        if(reusit==false){
            throw new NoSuchElementException("The client with id="+ id +" was not found!");
        }
    }

    public int insertClient(Client client) {
        for (Validator<Client> v : validators) {
            v.validate(client);
        }
        return ClientDAO.insert(client);
    }

    public ArrayList<Client> viewAll(){
        ArrayList<Client> clients=ClientDAO.viewAll();
        if(clients.size()==0)
            throw new NoSuchElementException("No clients");
        return clients;
    }

    public void deleteClient(int id){
        ClientDAO.deleteById(id);
    }
}