package Presentation;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class CreateTable {
    public static JTable createTable(ArrayList <? extends Object> lista){
        Object[][] obj=new Object[lista.size()][lista.get(0).getClass().getDeclaredFields().length];
        String[] columnName=new String[lista.get(0).getClass().getDeclaredFields().length];
        int i=0;
        for(Field field:lista.get(0).getClass().getDeclaredFields()){
            columnName[i++]=field.getName();
        }
        i=0;
        int j=0;
        for(Object c:lista){
            for(Field field:c.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    obj[i][j++]=field.get(c);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            j=0;
            i++;
        }
        JTable table=new JTable(obj,columnName);
        return table;
    }
}
