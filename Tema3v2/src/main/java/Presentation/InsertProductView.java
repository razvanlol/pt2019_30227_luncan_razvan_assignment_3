package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InsertProductView extends JFrame {
    private JPanel panel;

    private JTextField name;
    private JTextField id;
    private JTextField price;
    private JTextField quantity;

    private JLabel idLabel;
    private JLabel nameLabel;
    private JLabel priceLabel;
    private JLabel quantityLabel;

    private JButton insert;


    public InsertProductView(){
        this.panel=new JPanel();
        this.panel.setLayout(new GridLayout(9,1));
        this.id=new JTextField("id");
        this.name=new JTextField("Name");
        this.price=new JTextField("Price");
        this.quantity=new JTextField("Quantity");
        this.insert=new JButton("Insert");

        this.idLabel=new JLabel("ID");
        this.nameLabel=new JLabel("Name");
        this.priceLabel=new JLabel("Price");
        this.quantityLabel=new JLabel("Quantity");

        this.panel.add(idLabel);
        this.panel.add(id);
        this.panel.add(nameLabel);
        this.panel.add(name);
        this.panel.add(priceLabel);
        this.panel.add(price);
        this.panel.add(quantityLabel);
        this.panel.add(quantity);
        this.panel.add(insert);
        this.setSize(600,300);
        this.setContentPane(panel);

    }

    public JTextField getTfName() {
        return name;
    }

    public void setName(JTextField name) {
        this.name = name;
    }

    public JTextField getId() {
        return id;
    }

    public void setId(JTextField id) {
        this.id = id;
    }

    public JTextField getPrice() {
        return price;
    }

    public JTextField getQuantity() {
        return quantity;
    }

    public void setQuantity(JTextField quantity) {
        this.quantity = quantity;
    }

    public void insertControl(ActionListener a){
        insert.addActionListener(a);
    }

}
