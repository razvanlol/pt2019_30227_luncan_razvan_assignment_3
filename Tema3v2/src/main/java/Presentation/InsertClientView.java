package Presentation;

import Model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class InsertClientView extends JFrame {
    private JPanel panel;

    private JTextField name;
    private JTextField address;
    private JTextField email;
    private JTextField id;
    private JTextField age;

    private JLabel numeLabel;
    private JLabel adresaLabel;
    private JLabel emailLabel;
    private JLabel idLabel;
    private JLabel ageLabel;


    private JPanel panelTF;

    private JButton insert;

    public InsertClientView(){
        this.panel=new JPanel();
        this.name=new JTextField("");
        this.address=new JTextField("");
        this.email=new JTextField("");
        this.id=new JTextField("");
        this.age=new JTextField("");
        this.insert=new JButton("Insert");

        this.numeLabel=new JLabel("Nume");
        this.adresaLabel=new JLabel("Adresa");
        this.emailLabel=new JLabel("Email");
        this.idLabel=new JLabel("ID");
        this.ageLabel=new JLabel("Varsta");

        this.panelTF=new JPanel();

        this.panelTF.setLayout(new GridLayout(11,1));

        this.panelTF.add(numeLabel);
        this.panelTF.add(name);
        this.panelTF.add(adresaLabel);
        this.panelTF.add(address);
        this.panelTF.add(emailLabel);
        this.panelTF.add(email);
        this.panelTF.add(idLabel);
        this.panelTF.add(id);
        this.panelTF.add(ageLabel);
        this.panelTF.add(age);
        this.panelTF.add(insert);
        this.setSize(600,300);
        this.setContentPane(panelTF);

    }

    public JTextField getTfName() {
        return name;
    }

    public JTextField getAddress() {
        return address;
    }

    public JTextField getEmail() {
        return email;
    }

    public JTextField getId() {
        return id;
    }

    public JTextField getAge() {
        return age;
    }

    public void insertControl(ActionListener a){
        insert.addActionListener(a);
    }

}
