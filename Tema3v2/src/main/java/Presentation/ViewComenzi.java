package Presentation;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ViewComenzi extends JFrame {

    private JButton aplicaComanda;
    private JButton backButtonCom;

    private JPanel mainPanel;
    private JPanel panelOrderID;
    private JPanel panelCleintID;
    private JPanel panelProductID;
    private JPanel panelProductQuantity;
    private JPanel panelDay;
    private JPanel panelMonth;
    private JPanel panelYear;
    private JPanel buttons;


    private JLabel orderIDLabel;
    private JLabel clientIDLabel;
    private JLabel productIDLabel;
    private JLabel quantityLabel;
    private JLabel dayLabel;
    private JLabel monthLabel;
    private JLabel yearLabel;

    private JTextField orderIDTF;
    private JTextField clientIDTF;
    private JTextField productIDTF;
    private JTextField quantityTF;
    private JTextField dayTF;
    private JTextField monthTF;
    private JTextField yearTF;


    public ViewComenzi(){

        this.panelCleintID=new JPanel();
        this.panelOrderID=new JPanel();
        this.panelProductID=new JPanel();
        this.panelDay=new JPanel();
        this.panelMonth=new JPanel();
        this.panelYear=new JPanel();
        this.panelProductQuantity=new JPanel();
        this.mainPanel=new JPanel();
        this.buttons=new JPanel();

        this.buttons.setLayout(new BoxLayout(buttons,BoxLayout.X_AXIS));
        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.panelCleintID.setLayout(new BoxLayout(panelCleintID,BoxLayout.X_AXIS));
        this.panelOrderID.setLayout(new BoxLayout(panelOrderID,BoxLayout.X_AXIS));
        this.panelProductID.setLayout(new BoxLayout(panelProductID,BoxLayout.X_AXIS));
        this.panelDay.setLayout(new BoxLayout(panelDay,BoxLayout.X_AXIS));
        this.panelMonth.setLayout(new BoxLayout(panelMonth,BoxLayout.X_AXIS));
        this.panelYear.setLayout(new BoxLayout(panelYear,BoxLayout.X_AXIS));
        this.panelProductQuantity.setLayout(new BoxLayout(panelProductQuantity,BoxLayout.X_AXIS));

        this.clientIDLabel=new JLabel("Client ID             ");
        this.productIDLabel=new JLabel("Product ID         ");
        this.orderIDLabel=new JLabel("Order ID             ");
        this.quantityLabel=new JLabel("Quantity             ");
        this.dayLabel=new JLabel("Day                      ");
        this.monthLabel=new JLabel("Month                 ");
        this.yearLabel=new JLabel("Year                   ");

        this.orderIDTF=new JTextField();
        this.clientIDTF=new JTextField();
        this.productIDTF=new JTextField();
        this.quantityTF=new JTextField();
        this.dayTF=new JTextField();
        this.monthTF=new JTextField();
        this.yearTF=new JTextField();

        this.aplicaComanda=new JButton("Aplica Comanda");
        this.backButtonCom=new JButton("Back");

        this.panelCleintID.add(clientIDLabel);
        this.panelCleintID.add(clientIDTF);
        this.panelOrderID.add(orderIDLabel);
        this.panelOrderID.add(orderIDTF);
        this.panelProductID.add(productIDLabel);
        this.panelProductID.add(productIDTF);
        this.panelProductQuantity.add(quantityLabel);
        this.panelProductQuantity.add(quantityTF);
        this.panelDay.add(dayLabel);
        this.panelDay.add(dayTF);
        this.panelMonth.add(monthLabel);
        this.panelMonth.add(monthTF);
        this.panelYear.add(yearLabel);
        this.panelYear.add(yearTF);

        this.buttons.add(aplicaComanda);
        this.buttons.add(backButtonCom);

        this.mainPanel.add(panelOrderID);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelCleintID);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelProductID);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelDay);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelMonth);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelYear);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(panelProductQuantity);
        this.mainPanel.add(Box.createRigidArea(new Dimension(0,5)));
        this.mainPanel.add(buttons);

        this.setTitle("Clienti");
        this.setPreferredSize(new Dimension(500,700));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();

    }

    public void backButonComenzi(ActionListener a1){
        backButtonCom.addActionListener(a1);
    }


    public void aplicaButon(ActionListener a1){
        aplicaComanda.addActionListener(a1);
    }


    public JTextField getOrderIDTF() {
        return orderIDTF;
    }


    public JTextField getClientIDTF() {
        return clientIDTF;
    }


    public JTextField getProductIDTF() {
        return productIDTF;
    }


    public JTextField getQuantityTF() {
        return quantityTF;
    }


    public JTextField getDayTF() {
        return dayTF;
    }


    public JTextField getMonthTF() {
        return monthTF;
    }


    public JTextField getYearTF() {
        return yearTF;
    }

}