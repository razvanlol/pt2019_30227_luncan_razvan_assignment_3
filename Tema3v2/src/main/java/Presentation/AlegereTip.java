package Presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AlegereTip extends JFrame {

    private JButton client;
    private JButton produs;
    private JButton comanda;
    private JPanel panelButoane;

    public AlegereTip(){

        this.client=new JButton("Client");
        this.produs=new JButton("Produs");
        this.comanda=new JButton("Comanda");
        this.panelButoane=new JPanel();
        this.panelButoane.setLayout(new GridLayout(1,3));

        this.panelButoane.add(client);
        this.panelButoane.add(produs);
        this.panelButoane.add(comanda);

        this.setTitle("Simulator Cozi");
        this.setPreferredSize(new Dimension(500,100));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.setContentPane(panelButoane);
        this.pack();
    }

    public void actButonClienti(ActionListener a1){
        client.addActionListener(a1);
    }

    public void actButonProduse(ActionListener a2){produs.addActionListener(a2);}

    public void actButonComenzi(ActionListener a3){comanda.addActionListener(a3);}


}
