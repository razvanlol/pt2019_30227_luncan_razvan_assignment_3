package Presentation;

import Business.ClientBLL;
import Model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ViewClienti extends JFrame {

    private JButton butonAfiseazaTot;
    private JButton insertButton;
    private JButton updateButton;
    private JButton deleteButton;
    private JButton backButton;

    private JPanel panelLabelTF;
    private JPanel panelButoane;

    private JPanel mainPanel;
    private JPanel panelForLabelTFAndTable;
    private JPanel panelForTable;

    private JTable table;
    private JTable clientTable;

    private JScrollPane pane;



    public ViewClienti(){

        this.butonAfiseazaTot=new JButton("View All");
        this.insertButton=new JButton("Insert");
        this.updateButton=new JButton("Update");
        this.deleteButton=new JButton("Delete");
        this.backButton=new JButton("Back");


        this.panelLabelTF=new JPanel();
        this.panelButoane=new JPanel();
        this.mainPanel=new JPanel();
        this.panelForLabelTFAndTable=new JPanel();
        this.panelForTable=new JPanel();

        this.clientTable=new JTable();

        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.panelButoane.setLayout(new BoxLayout(panelButoane,BoxLayout.X_AXIS));
        this.panelLabelTF.setLayout(new BoxLayout(panelLabelTF,BoxLayout.Y_AXIS));
        this.panelForLabelTFAndTable.setLayout(new BoxLayout(panelForLabelTFAndTable,BoxLayout.X_AXIS));


        this.panelForTable.add(clientTable);

        this.panelForLabelTFAndTable.add(panelLabelTF);
        this.panelForLabelTFAndTable.add(panelLabelTF);

        this.panelButoane.add(butonAfiseazaTot);
        this.panelButoane.add(insertButton);
        this.panelButoane.add(updateButton);
        this.panelButoane.add(deleteButton);
        this.panelButoane.add(backButton);

        this.mainPanel.add(panelButoane);
        this.mainPanel.add(panelForLabelTFAndTable);
        createTable();
        this.setTitle("Clienti");
        this.setPreferredSize(new Dimension(500,700));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();
    }


    public void createTable(){
        ArrayList<Client> clients;
        ClientBLL cb=new ClientBLL();
        clients=cb.viewAll();
        this.table=CreateTable.createTable(clients);
        pane=new JScrollPane(table);
        mainPanel.add(pane);
    }

    public void removePane(){
        this.mainPanel.remove(this.pane);
    }

    public JTable getTable() {
        return table;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void  insertButonClienti(ActionListener a2){
        insertButton.addActionListener(a2);
    }

    public void backButonClienti(ActionListener a1){
        backButton.addActionListener(a1);
    }

    public void deleteButonClienti(ActionListener a){
        deleteButton.addActionListener(a);
    }

    public void updateButonClienti(ActionListener a){
        updateButton.addActionListener(a);
    }

    public void viewAllClienti(ActionListener a){
        butonAfiseazaTot.addActionListener(a);
    }






}
