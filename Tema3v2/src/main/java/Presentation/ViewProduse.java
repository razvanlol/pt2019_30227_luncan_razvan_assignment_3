package Presentation;

import Business.ClientBLL;
import Business.ProductBLL;
import Model.Client;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;

public class ViewProduse extends JFrame {

    private JButton butonAfiseazaTot;
    private JButton insertButton;
    private JButton updateButton;
    private JButton deleteButton;
    private JButton backButton;

    private JPanel panelLabelTF;
    private JPanel panelButoane;

    private JPanel mainPanel;
    private JPanel panelForLabelTFAndTable;
    private JPanel panelForTable;

    private JTable table;
    private JTable clientTable;

    private JScrollPane pane;


    public ViewProduse(){

        ProductBLL cb=new ProductBLL();

        this.butonAfiseazaTot=new JButton("View All");
        this.insertButton=new JButton("Insert");
        this.updateButton=new JButton("Update");
        this.deleteButton=new JButton("Delete");
        this.backButton=new JButton("Back");


        this.panelLabelTF=new JPanel();
        this.panelButoane=new JPanel();
        this.mainPanel=new JPanel();
        this.panelForLabelTFAndTable=new JPanel();
        this.panelForTable=new JPanel();

        this.clientTable=new JTable();

        this.mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
        this.panelButoane.setLayout(new BoxLayout(panelButoane,BoxLayout.X_AXIS));
        this.panelLabelTF.setLayout(new BoxLayout(panelLabelTF,BoxLayout.Y_AXIS));
        this.panelForLabelTFAndTable.setLayout(new BoxLayout(panelForLabelTFAndTable,BoxLayout.X_AXIS));


        this.panelForTable.add(clientTable);

        this.panelForLabelTFAndTable.add(panelLabelTF);
        this.panelForLabelTFAndTable.add(panelLabelTF);

        this.panelButoane.add(butonAfiseazaTot);
        this.panelButoane.add(insertButton);
        this.panelButoane.add(updateButton);
        this.panelButoane.add(deleteButton);
        this.panelButoane.add(backButton);

        this.mainPanel.add(panelButoane);
        this.mainPanel.add(panelForLabelTFAndTable);

        createTable();

        this.setTitle("Product");
        this.setPreferredSize(new Dimension(500,700));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(false);
        this.setContentPane(mainPanel);
        this.pack();
    }

    public void createTable(){
        ArrayList<Product> products;
        ProductBLL cb=new ProductBLL();
        products=cb.viewAll();
        this.table=CreateTable.createTable(products);
        pane=new JScrollPane(table);
        mainPanel.add(pane);
    }


    /*public void createTable(){
        ProductBLL cb=new ProductBLL();
        ArrayList<Product> products=cb.viewAll();
        Object[][] obj=new Object[products.size()][products.get(0).getClass().getDeclaredFields().length];
        String[] columnName=new String[products.get(0).getClass().getDeclaredFields().length];
        int i=0;
        for(Field field:products.get(0).getClass().getDeclaredFields()){
            columnName[i++]=field.getName();
        }
        i=0;
        int j=0;
        for(Product c:products){
            for(Field field:c.getClass().getDeclaredFields()){
                field.setAccessible(true);
                try {
                    obj[i][j++]=field.get(c);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            j=0;
            i++;
        }
        table=new JTable(obj,columnName);
        pane=new JScrollPane(table);
        mainPanel.add(pane);
    }
    */

    public JTable getTable() {
        return table;
    }

    public JScrollPane getPane() {
        return pane;
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void  insertButonProduse(ActionListener a2){
        insertButton.addActionListener(a2);
    }

    public void backButonProduse(ActionListener a1){
        backButton.addActionListener(a1);
    }

    public void deleteButonProduse(ActionListener a){
        deleteButton.addActionListener(a);
    }

    public void updateButonProduse(ActionListener a){
        updateButton.addActionListener(a);
    }

    public void viewAllProduse(ActionListener a){
        butonAfiseazaTot.addActionListener(a);
    }






}
