package Controller;

import Business.ClientBLL;
import Business.OrderBLL;
import Business.ProductBLL;
import Model.Client;
import Model.Order;
import Model.Product;
import Presentation.*;
import com.mysql.cj.x.protobuf.MysqlxCrud;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Controller {

    private AlegereTip alegereTip;
    private ViewClienti viewClienti;
    private ViewProduse viewProduse;
    private ViewComenzi viewComenzi;
    private InsertProductView insertProductView;
    private InsertClientView insertClientView;

    public Controller(AlegereTip alegereTip,ViewClienti viewClienti,ViewProduse viewProduse,ViewComenzi viewComenzi,InsertClientView insertClientView,InsertProductView insertProductView){

        this.insertClientView=insertClientView;
        this.insertClientView.insertControl(new controlInsert());
        this.insertProductView=insertProductView;
        this.insertProductView.insertControl(new controlInsertProduse());
        this.alegereTip=alegereTip;
        this.alegereTip.actButonClienti(new butonClienti());
        this.alegereTip.actButonProduse(new butonProduse());
        this.alegereTip.actButonComenzi(new butonComenzi());
        this.viewClienti=viewClienti;
        this.viewClienti.backButonClienti(new butonBackC());
        this.viewClienti.insertButonClienti(new butonInsertClienti());
        this.viewClienti.deleteButonClienti(new butonDeleteClienti());
        this.viewClienti.updateButonClienti(new butonUpdateClienti());
        this.viewClienti.viewAllClienti(new butonViewAllClienti());
        this.viewProduse=viewProduse;
        this.viewProduse.backButonProduse(new butonBackP());
        this.viewProduse.insertButonProduse(new butonInsertProduse());
        this.viewProduse.deleteButonProduse(new butonDeleteProduse());
        this.viewProduse.updateButonProduse(new butonUpdateProduse());
        this.viewProduse.viewAllProduse(new butonViewAllProduse());
        this.viewComenzi=viewComenzi;
        this.viewComenzi.backButonComenzi(new butonBackCom());
        this.viewComenzi.aplicaButon(new butonAplicaCom());
    }

    public class butonClienti implements ActionListener{

      //  @Override
        public void actionPerformed(ActionEvent e) {
            viewClienti.setVisible(true);
            alegereTip.dispose();
        }
    }

    public class butonProduse implements ActionListener{

       // @Override
        public void actionPerformed(ActionEvent e) {
            viewProduse.setVisible(true);
            alegereTip.dispose();
        }
    }

    public class butonComenzi implements ActionListener{
      //  @Override
        public void actionPerformed(ActionEvent e) {
            viewComenzi.setVisible(true);
            alegereTip.dispose();
        }
    }

    public class butonBackC implements ActionListener{

      //  @Override
        public void actionPerformed(ActionEvent e) {
            alegereTip.setVisible(true);
            viewClienti.dispose();
        }
    }

    public class butonBackP implements ActionListener{

       // @Override
        public void actionPerformed(ActionEvent e) {
            alegereTip.setVisible(true);
            viewProduse.dispose();
        }
    }

    public class butonBackCom implements ActionListener{

      //  @Override
        public void actionPerformed(ActionEvent e) {
            alegereTip.setVisible(true);
            viewComenzi.dispose();
        }
    }

    public class butonInsertClienti implements ActionListener{

      //  @Override
        public void actionPerformed(ActionEvent e) {
            insertClientView.setVisible(true);
            viewClienti.dispose();
        }
    }

    public class butonDeleteClienti implements ActionListener{

       // @Override
        public void actionPerformed(ActionEvent e) {
            ClientBLL cb=new ClientBLL();
            int id=Integer.parseInt(String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 3)));
            cb.deleteClient(id);
        }
    }

    public class butonUpdateClienti implements ActionListener{

       // @Override
        public void actionPerformed(ActionEvent e) {
            ClientBLL cb=new ClientBLL();
            String name=String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 0));
            String address=String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 1));
            String email=String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 2));
            int id=Integer.parseInt(String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 3)));
            int age=Integer.parseInt(String.valueOf(viewClienti.getTable().getModel().getValueAt(viewClienti.getTable().getSelectedRow(), 4)));
            cb.updateClient(id,name,address,email,age);

        }
    }

    public class butonViewAllClienti implements ActionListener{


        //@Override
        public void actionPerformed(ActionEvent e) {
            viewClienti.removePane();
            viewClienti.createTable();
            viewClienti.getMainPanel().revalidate();
        }
    }

    public class controlInsert implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String name= insertClientView.getTfName().getText();
            String address=insertClientView.getAddress().getText();
            String email=insertClientView.getEmail().getText();
            int id=Integer.parseInt(insertClientView.getId().getText());
            int age=Integer.parseInt(insertClientView.getAge().getText());
            Client c=new Client(name,address,email,id,age);
            ClientBLL cb=new ClientBLL();
            cb.insertClient(c);
            insertClientView.dispose();
            viewClienti.setVisible(true);
        }
    }

    public class butonInsertProduse implements ActionListener{

        //@Override
        public void actionPerformed(ActionEvent e) {
            insertProductView.setVisible(true);
            viewProduse.dispose();
        }
    }

    public class butonDeleteProduse implements ActionListener{

        //@Override
        public void actionPerformed(ActionEvent e) {
            ProductBLL cb=new ProductBLL();
            int id=Integer.parseInt(String.valueOf(viewProduse.getTable().getModel().getValueAt(viewProduse.getTable().getSelectedRow(), 0)));
            cb.deleteProduct(id);
        }
    }

    public class butonUpdateProduse implements ActionListener{

        //@Override
        public void actionPerformed(ActionEvent e) {
            ProductBLL cb=new ProductBLL();
            int id=Integer.parseInt(String.valueOf(viewProduse.getTable().getModel().getValueAt(viewProduse.getTable().getSelectedRow(), 0)));
            String name=String.valueOf(viewProduse.getTable().getModel().getValueAt(viewProduse.getTable().getSelectedRow(), 1));
            int price=Integer.parseInt(String.valueOf(viewProduse.getTable().getModel().getValueAt(viewProduse.getTable().getSelectedRow(), 2)));
            int quantity=Integer.parseInt(String.valueOf(viewProduse.getTable().getModel().getValueAt(viewProduse.getTable().getSelectedRow(), 3)));

            cb.updateProduct(id,name,price,quantity);

        }
    }

    public class butonViewAllProduse implements ActionListener{


        //@Override
        public void actionPerformed(ActionEvent e) {
            viewProduse.getMainPanel().remove(viewProduse.getPane());
            viewProduse.createTable();
            viewProduse.getMainPanel().revalidate();
        }
    }

    public class controlInsertProduse implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String name= insertProductView.getTfName().getText();
            int id=Integer.parseInt(insertProductView.getId().getText());
            int price=Integer.parseInt(insertProductView.getPrice().getText());
            int quantity=Integer.parseInt(insertProductView.getQuantity().getText());
            Product c=new Product(id,name,price,quantity);
            ProductBLL cb=new ProductBLL();
            cb.insertProduct(c);
            insertProductView.dispose();
            viewProduse.setVisible(true);
        }
    }

    public class butonAplicaCom implements ActionListener {

        public void actionPerformed(ActionEvent e){

            int id=Integer.parseInt(viewComenzi.getOrderIDTF().getText());
            int idClient=Integer.parseInt(viewComenzi.getClientIDTF().getText());
            int idProdus=Integer.parseInt(viewComenzi.getProductIDTF().getText());
            int day=Integer.parseInt(viewComenzi.getDayTF().getText());
            int month=Integer.parseInt(viewComenzi.getMonthTF().getText());
            int year=Integer.parseInt(viewComenzi.getYearTF().getText());
            int quantity=Integer.parseInt(viewComenzi.getQuantityTF().getText());
            Order c=new Order(id,idClient,idProdus,day,month,year,quantity);
            ProductBLL cb=new ProductBLL();
            OrderBLL ob=new OrderBLL();
            ClientBLL ccb=new ClientBLL();
            if(cb.findProductById(idProdus).getQuantity()<quantity) {
                JOptionPane.showMessageDialog(viewComenzi, "Invalid Quantity");
            }
            else {
                ob.insertOrder(c);
                Product p=cb.findProductById(c.getIdProduct());
                p.setQuantity(p.getQuantity()-c.getQuantity());
                cb.updateProduct(p.getId(),p.getName(),p.getPrice(),p.getQuantity());
                List<String> lines = Arrays.asList(ccb.findClientById(c.getIdClient()).toString(), cb.findProductById(c.getIdProduct()).toString(), c.toString());
                Path file = Paths.get("order"+c.getId()+".txt");
                try {
                    Files.write(file, lines, Charset.forName("UTF-8"));
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                viewComenzi.dispose();
                alegereTip.setVisible(true);
            }
        }
    }




}
