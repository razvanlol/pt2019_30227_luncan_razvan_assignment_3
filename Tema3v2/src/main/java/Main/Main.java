package Main;

import Connection.ConnectionFactory;
import Controller.Controller;
import Presentation.*;

public class Main {
    public static void main(String[] args) {
        AlegereTip alegereTip=new AlegereTip();
        ViewClienti viewClienti=new ViewClienti();
        ViewProduse viewProduse=new ViewProduse();
        ViewComenzi viewComenzi=new ViewComenzi();
        InsertClientView insertClientView=new InsertClientView();
        InsertProductView insertProductView=new InsertProductView();
        Controller controller=new Controller(alegereTip,viewClienti,viewProduse,viewComenzi,insertClientView,insertProductView);

    }
}
