package Model;

public class Order {

    private int id;
    private int idClient;
    private int idProduct;
    private int day;
    private int month;
    private int year;
    private int quantity;

    public Order(int id,int idClient,int idProduct,int day,int month,int year,int quantity){

        this.id=id;
        this.idClient=idClient;
        this.idProduct=idProduct;
        this.day=day;
        this.month=month;
        this.year=year;
        this.quantity=quantity;
    }

    public int getId() {
        return id;
    }

    public int getIdClient() {
        return idClient;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", idClient=" + idClient +
                ", idProduct=" + idProduct +
                ", day=" + day +
                ", month=" + month +
                ", year=" + year +
                ", quantity=" + quantity +
                '}';
    }
}
