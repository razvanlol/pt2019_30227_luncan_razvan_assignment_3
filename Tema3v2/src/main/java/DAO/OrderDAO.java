package DAO;

import Model.Order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import Connection.*;

public class OrderDAO {


    private static final String inserStatementString="INSERT INTO ordertable(id,idClient,idProduct,day,month,year,quantity) VALUES(?,?,?,?,?,?,?)";

    /**
     * Metoda insert insereaza un order in baza de date
     * @param order Reprezinta un prder ce va fi inserat in baza de date
     * @return Returneaza id-ul orderului care a fost inserat
     */

    public static int insert(Order order){

        Connection dbConnection= ConnectionFactory.getConnection();
        PreparedStatement insertStatement=null;
        int insertedId=-1;

        try{
            insertStatement=dbConnection.prepareStatement(inserStatementString);
            insertStatement.setInt(1,order.getId());
            insertStatement.setInt(2,order.getIdClient());
            insertStatement.setInt(3,order.getIdProduct());
            insertStatement.setInt(4,order.getDay());
            insertStatement.setInt(5,order.getMonth());
            insertStatement.setInt(6,order.getYear());
            insertStatement.setInt(7,order.getQuantity());
            insertStatement.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }


}
