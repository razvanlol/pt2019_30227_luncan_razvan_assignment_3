package DAO;

import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Connection.ConnectionFactory;

public class ClientDAO {

    protected static final Logger LOGGER= Logger.getLogger(ClientDAO.class.getName());
    private static final String inserStatementString="INSERT INTO client(id,name,address,email,age) VALUES(?,?,?,?,?)";
    private final static  String findStatementString="SELECT * FROM client where id= ?";
    private static final String updateStatemtString="UPDATE client SET name=?,address=?,email=?,age=? WHERE id=?";
    private final static  String viewAllStatementString="SELECT * FROM client";
    private final static  String deleteStatementString="DELETE FROM client WHERE id=?";


    /**
     * Returneaza un client din baza de date care a fost cautat dupa ID
     * @param id Reprezinta id-ul dupa care cautam clientul
     * @return metoda va returna un client
     */
    public static Client findById(int id){

        Client client=null;

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement findStatment=null;
        ResultSet rs=null;
        try{
            findStatment=dbConnection.prepareStatement(findStatementString);
            findStatment.setLong(1,id);
            rs=findStatment.executeQuery();
            rs.next();

            String name=rs.getString("name");
            String address=rs.getString("address");
            String email=rs.getString("email");
            int age=rs.getInt("age");
            client=new Client(name,address,email,id,age);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatment);
            ConnectionFactory.close(dbConnection);
        }
        return client;
    }

    /**
     * Metoda modifica un client dupa id
     * @param id Reprezinta id-ul clientului care va fi modificat
     * @param name Cum se va numi clientul
     * @param address Care va fi noua lui adresa
     * @param email Care va fi noul lui email
     * @param age Care va fi noua lui varsta
     * @return Aceasta metoda nu retuneaza nimic
     */
    public static boolean updateById(int id,String name,String address,String email,int age){

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement updateStaement=null;
        boolean status=false;
        try{
            updateStaement=dbConnection.prepareStatement(updateStatemtString);
            updateStaement.setString(1,name);
            updateStaement.setString(2,address);
            updateStaement.setString(3,email);
            updateStaement.setInt(4,age);
            updateStaement.setInt(5,id);
            updateStaement.executeUpdate();
            status=true;

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:update "+e.getMessage());
        }finally {
            ConnectionFactory.close(updateStaement);
            ConnectionFactory.close(dbConnection);
        }
        return status;
    }

    /**
     * Metoda insert insereaza un client in baza de date
     * @param client Reprezinta un client ce va fi inserat in baza de date
     * @return Returneaza id-ul clientului care a fost inserat
     */
    public static int insert(Client client){

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement insertStatement=null;
        int insertedId=-1;

        try{
            insertStatement=dbConnection.prepareStatement(inserStatementString);
            insertStatement.setInt(1,client.getId());
            insertStatement.setString(2,client.getName());
            insertStatement.setString(3,client.getAddress());
            insertStatement.setString(4,client.getEmail());
            insertStatement.setInt(5,client.getAge());
            insertStatement.executeUpdate();

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:insert "+e.getMessage());
        }finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }


    /**
     * Metoda view all returneaza toti clientii din baza de date
     * @return Aceasta metoda returneaza toti clientii din baza de date pentru a fi afisati in tabela
     */
    public static ArrayList<Client> viewAll(){

        ArrayList<Client> clients=new ArrayList<Client>();

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement viewStatement=null;

        ResultSet rs=null;
        try{
            viewStatement=dbConnection.prepareStatement(viewAllStatementString);
            rs=viewStatement.executeQuery();
            while(rs.next()) {
                String name = rs.getString("name");
                String address = rs.getString("address");
                String email = rs.getString("email");
                int id = rs.getInt("id");
                int age = rs.getInt("age");
                Client client=new Client(name,address,email,id,age);
                clients.add(client);
            }
        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(viewStatement);
            ConnectionFactory.close(dbConnection);
        }
        return clients;
    }

    /**
     * Metoda sterge un client dupa id-ul sau
     * @param id Reprezinta id-ul clientului care va fi sters
     */
    public static void deleteById(int id){


        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement deleteStatement=null;

        try{
            deleteStatement=dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setLong(1,id);
            deleteStatement.executeUpdate();

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
