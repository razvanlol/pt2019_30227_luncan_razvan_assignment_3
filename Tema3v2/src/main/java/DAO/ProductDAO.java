package DAO;

import Model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import Connection.ConnectionFactory;

public class ProductDAO {

    protected static final Logger LOGGER= Logger.getLogger(ClientDAO.class.getName());
    private static final String inserStatementString="INSERT INTO product(id,name,price,quantity) VALUES(?,?,?,?)";
    private final static  String findStatementString="SELECT * FROM product where id= ?";
    private static final String updateStatemtString="UPDATE product SET name=?,price=?,quantity=? WHERE id=?";
    private final static  String viewAllStatementString="SELECT * FROM product";
    private final static  String deleteStatementString="DELETE FROM product WHERE id=?";

    /**
     * Returneaza un produs din baza de date care a fost cautat dupa ID
     * @param id Reprezinta id-ul dupa care cautam produsul
     * @return metoda va returna un produs
     */

    public static Product findById(int id){

        Product product=null;

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement findStatment=null;
        ResultSet rs=null;
        try{
            findStatment=dbConnection.prepareStatement(findStatementString);
            findStatment.setLong(1,id);
            rs=findStatment.executeQuery();
            rs.next();

            String name=rs.getString("name");
            int price=rs.getInt("price");
            int quantity=rs.getInt("quantity");
            product=new Product(id,name,price,quantity);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ProductDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatment);
            ConnectionFactory.close(dbConnection);
        }
        return product;
    }

    /**
     * Metoda modifica un client dupa id
     * @param id Reprezinta id-ul produsului care va fi modificat
     * @param name Cum se va numi produsul
     * @param price Care va fi noul lui pret
     * @param quantity Care va fi noua cantitate
     * @return Aceasta metoda un boolean care spune daca update-ul s-a efectuat cu succes
     */

    public static boolean updateById(int id,String name,int price,int quantity){

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement updateStaement=null;
        boolean status=false;
        try{
            updateStaement=dbConnection.prepareStatement(updateStatemtString);

            updateStaement.setString(1,name);
            updateStaement.setInt(2,price);
            updateStaement.setInt(3,quantity);
            updateStaement.setInt(4,id);
            updateStaement.executeUpdate();
            status=true;

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ProductDAO:update "+e.getMessage());
        }finally {
            ConnectionFactory.close(updateStaement);
            ConnectionFactory.close(dbConnection);
        }
        return status;
    }

    /**
     * Metoda insert insereaza un produs in baza de date
     * @param product Reprezinta un produs ce va fi inserat in baza de date
     * @return Returneaza id-ul produsului care a fost inserat
     */

    public static int insert(Product product){

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement insertStatement=null;
        int insertedId=-1;

        try{
            insertStatement=dbConnection.prepareStatement(inserStatementString);
            insertStatement.setInt(1,product.getId());
            insertStatement.setString(2,product.getName());
            insertStatement.setInt(3,product.getPrice());
            insertStatement.setInt(4,product.getQuantity());
            insertStatement.executeUpdate();

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:insert "+e.getMessage());
        }finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    /**
     * Metoda view all returneaza toate produsele din baza de date
     * @return Aceasta metoda returneaza toate produsele din baza de date pentru a fi afisati in tabela
     */

    public static ArrayList<Product> viewAll(){

        ArrayList<Product> products=new ArrayList<Product>();

        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement viewStatement=null;
        ResultSet rs=null;
        try{
            viewStatement=dbConnection.prepareStatement(viewAllStatementString);
            rs=viewStatement.executeQuery();
            while(rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int price = rs.getInt("price");
                int quantity = rs.getInt("quantity");
                Product product=new Product(id,name,price,quantity);
                products.add(product);
            }
        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ClientDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(viewStatement);
            ConnectionFactory.close(dbConnection);
        }
        return products;
    }

    /**
     * Metoda sterge un produs dupa id-ul sau
     * @param id Reprezinta id-ul produsului care va fi sters
     */

    public static void deleteById(int id){


        Connection dbConnection=ConnectionFactory.getConnection();
        PreparedStatement deleteStatement=null;

        try{
            deleteStatement=dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setLong(1,id);
            deleteStatement.executeUpdate();

        }catch (SQLException e){
            LOGGER.log(Level.WARNING,"ProductDAO:findById "+e.getMessage());
        }finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
}
